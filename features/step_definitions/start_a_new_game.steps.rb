Given("I am on the home page") do
  visit root_path
 	click_on 'Grid test'
  #sleep(1) 
end

When("I click on the new link") do
  click_on 'New Game'
  #sleep(2)
end

Then("I should be taken to the new game page to enter the player names") do
  expect(page).to have_content "New Game"
end