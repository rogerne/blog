Feature: Deploy my fleet automatically
As a user
If I have the auto deploy checkbox selected
I want the system to deploy all my vessels for me

Scenario: auto-deploy the fleet
Given I have the auto deploy checkbox selected
When I click on Deploy Vessel
Then I should see "Your Fleet is deployed. Press Play to continue."
