class AddOceanGridHistoryToGames < ActiveRecord::Migration[5.2]
  def change
  	add_column :games, :p1_og_history, :text
    add_column :games, :p2_og_history, :text
  end
end
