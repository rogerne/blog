class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :pA_name
      t.string :pB_name

      t.timestamps
    end
  end
end
