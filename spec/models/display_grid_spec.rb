require 'rails_helper'

RSpec.describe DisplayGrid, type: :model do

  #before(:context) do
   before(:each) do
    @g = DisplayGrid.new 10
  end	

	context "When initialized" do

		it "should provide me with a player grid of the correct size" do
			expect(@g.show_grid_row 1).to eq "X X X X X X X X X X"
		end

		it "should accept hits" do
			expect(@g.can_take_hit?("A3")).to eq true
		end
	end

	context "When a grid reference is hit" do
    
    it 'it should display new value and not accept hit in the same place' do
    	@g.update_grid("A3", "#","AC")
    	expect(@g.show_grid_row 1).to eq "X X # X X X X X X X"
    	expect(@g.can_take_hit?("A3")).to eq false
    end
	end

  context "When a vessel is sunk" do
    it 'should replace the display with vessel specific letters when a vertical vessel is sunk' do
      @g.update_grid "B2", "H", "CR" 
      @g.update_grid "B3", "H", "CR" 
      @g.update_grid "B4", "H", "CR" 
      expect(@g.show_grid_row 2).to eq "X H H H X X X X X X"
      @g.update_grid "B5", "S", "CR" 
      expect(@g.show_grid_row 2).to eq "X S S S S X X X X X"
    end

    it 'should replace the display with vessel specific letters when a horizontal vessel is sunk' do
      @g.update_grid "D2", "H", "SU" 
      @g.update_grid "E2", "H", "SU" 
      expect(@g.show_grid_row 4).to eq "X H X X X X X X X X"
      expect(@g.show_grid_row 5).to eq "X H X X X X X X X X"
      expect(@g.show_grid_row 6).to eq "X X X X X X X X X X"
      @g.update_grid "F2", "S", "SU" 
      expect(@g.show_grid_row 4).to eq "X S X X X X X X X X"
      expect(@g.show_grid_row 5).to eq "X S X X X X X X X X"
      expect(@g.show_grid_row 6).to eq "X S X X X X X X X X"
    end
  end

	context "When owned by a PC should generate a valid hit" do
    
    it 'it should display new value and not accept hit in the same place' do
    	ref = @g.get_play
    	#puts "ref = #{ref}"
    	expect(@g.is_valid_ref?(ref)).to eq true
    end
	end
end