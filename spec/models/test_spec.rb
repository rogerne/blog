require 'rails_helper'

RSpec.describe 'Game plays itself', type: :feature do

	context "When intialized" do

		it 'should do something' do
			@letters = Hash.new
      @numbers = Hash.new
      fill_letters 10
      
			g = DisplayGrid.new 10
			g.update_grid("D8", "H", "AC")
			g.update_grid("C8", "H", "AC")
			g.update_grid("B8", "H", "AC")

			vessel_history = g.history["AC"]
			puts "vessel_history #{vessel_history}"
			#sorted_history = history.values.sort
			#sorted_history = vessel_history.values.sort
			#sorted_history = vessel_history.sort_by{|s| s.scan(/\d+/).map{|s| s.to_i}}.reverse
			sorted_history = my_sort vessel_history.values
			#sorted_history = vessel_history.values.sort_by {|k, v| v}.reverse
			puts "sorted_history #{sorted_history}"
			ref1 = sorted_history[0]
			puts "ref1 #{ref1}"
			x1 = get_x_int(ref1)
			y1 = get_y_int(ref1) 
			#ref2 = sorted_history[history.length - 1]
			ref2 = sorted_history[sorted_history.length-1]
			puts "history.length #{sorted_history.length-1}"
			puts "ref2 #{ref2}"    
			x2 = get_x_int(ref2)
			y2 = get_y_int(ref2)
		end 
	end

	  def get_x_int(ref)
    co = get_co_ords(ref)
    x = @numbers[co["x"]]
  end

  def get_y_int(ref)
      co = get_co_ords(ref)
      y = co["y"].to_i
  end

  def get_co_ords(grid_ref)
    co_ords = Hash.new
    co_ords["x"] = grid_ref.byteslice(0).upcase
    co_ords["y"] = grid_ref.byteslice(1,grid_ref.size).to_i
    return co_ords
  end

    def fill_letters(size)
    i = 1
    ("A".."Z").each {|let|
      @letters[i] = let
      @numbers[let] = i
      i += 1
      break unless i <= size
    }
  end

  def my_sort(arr)
    if arr[0][0] == arr[1][0]
      return arr.sort { |x, y| x[1,x.size-1].to_f <=> y[1,y.size-1].to_f }
    else
      return arr.sort
    end
  end
end
