require 'rails_helper'
 
RSpec.describe 'New game', type: :feature do
  scenario 'index page' do
    visit root_path
    #sleep(1)
    click_on 'Grid test'
    click_on 'New Game'
    expect(page).to have_content('New Game')
    #sleep(1)
  end

  scenario 'index page' do
    visit root_path
    click_on 'Grid test'
    sleep(1)
    click_on 'New Game'
    sleep(1)
    fill_in('playerA', :with => 'John')
    fill_in('playerB', :with => 'Doe')
    sleep(3)
    click_on 'Create Game'
    expect(page).to have_content('Deploy your Aircraft Carrier')
    expect(page).to have_content('Player A = John').or have_content('Player A = Doe')
    expect(page).to have_content('Player B = John').or have_content('Player B = Doe')
    sleep(1)
  end

end
