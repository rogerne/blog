require 'rails_helper'
 
RSpec.describe 'Game plays itself', type: :feature do
  scenario 'run the game' do
  visit root_path
  click_on 'Grid test'
  click_on 'New Game'
  fill_in('playerA', :with => 'John')
  fill_in('playerB', :with => 'Doe')
  sleep(1)
  click_on 'Create Game'
  page.check('checkbox_id')
  click_on 'Deploy Vessel'
  click_on 'Play'
  player  =  page.find_by_id('curr-player').text
  puts "player = #{player}"
  #find_element(:id, 'demo-div')
  if player == "PlayerA"
    page.select("B", :from => "game_x_pos")
    page.select(2, :from => "game_y_pos")
  end
  sleep(3)
  end
end