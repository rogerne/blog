class DisplayGrid < Grid
attr_accessor :history

 def initialize(size)
  super(size)
   #@history = Hash.new
   @history = {"AC" => {},"BA" => {},"CR" => {},"SU" => {},"DE" => {}}
 end

public
 def can_take_hit?(ref)
  co = get_co_ords(ref)
   case @grid[numbers[co["x"]] - 1 ][co["y"] - 1 ].upcase
    when "X"
     return true
    else
     return false
   end
 end

  def update_grid(ref, result, vessel_type)
    co = get_co_ords(ref)
    @grid[numbers[co["x"]] - 1 ][co["y"] - 1 ] = result 
    case result
    when "H"
      #history[history.length] = ref
      #puts @history
      vessel_history = history[vessel_type]
      vessel_history[vessel_history.length] = ref
      #puts  "@history = #{@history}" 
    
    when "S"
      sunk_display = "S"
      #puts "Last cell #{ref} #{numbers[co["x"]] - 1 }:#{co["y"] - 1 }"
      co = get_co_ords(ref)
     #@grid[numbers[co["x"]] - 1 ][co["y"] - 1 ] = vessel_code vessel_type
      @grid[numbers[co["x"]] - 1 ][co["y"] - 1 ] = sunk_display 
      #puts "vessel_history #{history[vessel_type]}"
      history[vessel_type].each do |x, y|
      #puts "History #{y}"
      co = get_co_ords(y)
      #@grid[numbers[co["x"]] - 1 ][co["y"] - 1 ] = vessel_code vessel_type
      @grid[numbers[co["x"]] - 1 ][co["y"] - 1 ] = sunk_display        
      end
      history[vessel_type].clear

    end  
      fill_grid_container       
  end 

  def get_play
   count = 1 
   #puts "get_play #{count}"
   loop do
    sorted_history = history.sort_by {|k,v| v.length}.reverse
    #puts "size = #{sorted_history.first[1].length}"
    #case history.sort[0].size
    
    case sorted_history.first[1].length
    when 0
      @ref = random_ref
    when 1
      #puts "vessel 1 = #{sorted_history.first[0]}"
      @ref = post_hit_ref sorted_history.first[0]
    else
      #puts "dir known"
      #puts "vessel else = #{sorted_history.first[0]}"      
      @ref = dir_known_hit_ref sorted_history.first[0]
    end
        
    if can_take_hit?(@ref)
     return @ref
     break
    end
   count += 1
   end
    return @ref
  end

private
  def vessel_code(type)
    case type
    when "AC"
      @display_code = "A"
    when "BA"
      @display_code = "B"    
    when "CR"
      @display_code = "C"
    when "SU"
      @display_code = "U"
    when "DE"
      @display_code = "D"  
    end
  end

  def random_ref
   x = rand(1..@grid.size)
   x = @letters[x]
   y = rand(1..@grid.size)
   @ref = x + y.to_s
  end

  # Establishing orientation of vessel
  # After an initial hit the next hit should be a valid ref
  # randomly selected one move up, down, left or right. This
  # method will be used until a second hit or a sinking 
  # (when all hit records will be removed)
  def post_hit_ref(target_vessel)
    last_ref = history[target_vessel][0]
    #puts "last_ref #{last_ref}"
    co = get_co_ords(last_ref)
    x = numbers[co["x"]]
    y = co["y"].to_i
    dir = rand(0..1)
    increment = rand(0..1)
    case dir
    when 0
     case increment
      when 0
       x += 1 if x < @grid.size
      when 1
       x -= 1 if x > 1
      end
    when 1
     case increment
      when 0
       y += 1 if y < @grid.size
      when 1
       y -= 1 if y > 1
      end
    end
    @ref = letters[x] + y.to_s
  end

  # with 2 hits in a row or more, the next hit should be random in 
  # the direction of the 2 hits until another hit or a sinking
  # (when all hit records will be removed). Hint if the hits are in
  # order in the Hash then use the 1st & last entries to establish next
  def dir_known_hit_ref(target_vessel)
    vessel_history = history[target_vessel]
    puts "vessel_history #{vessel_history}"
    #sorted_history = history.values.sort
    #sorted_history = vessel_history.values.sort
    #sorted_history = vessel_history.values.sort_by {|k, v| v}.reverse
    puts "vessel_history.values #{vessel_history.values}"
    sorted_history = my_sort vessel_history.values
    puts "sorted_history #{sorted_history}"
    ref1 = sorted_history[0]
    puts "ref1 #{ref1}"
    x1 = get_x_int(ref1)
    y1 = get_y_int(ref1) 
    #ref2 = sorted_history[history.length - 1]
    ref2 = sorted_history[sorted_history.length-1]
    puts "history.length #{sorted_history.length-1}"
    puts "ref2 #{ref2}"    
    x2 = get_x_int(ref2)
    y2 = get_y_int(ref2)

    if x1 == x2 
      orientation = "Horizontal" 
    else 
      orientation = "Vertical" 
    end
    puts "orientation #{orientation}"

    first_or_last = rand(0..1)
    puts "first_or_last #{first_or_last}"

    case orientation
    when "Vertical"
      case first_or_last
      when 0
        x1 -= 1 if x1 > 1
        @ref = letters[x1]+y1.to_s
      when 1
        x2 += 1 if x2 < @grid.size
        @ref = letters[x2]+y2.to_s
      end
    when "Horizontal"
      case first_or_last
      when 0
        y1 -= 1 if y1 > 1
        @ref = letters[x1]+y1.to_s
      when 1
        y2 += 1 if y2 < @grid.size
        @ref = letters[x2]+y2.to_s
      end
    end
    puts "@ref #{@ref}"
    return @ref
  end
  

  def my_sort(arr)
    if arr[0][0] == arr[1][0]
      return arr.sort { |x, y| x[1,x.size-1].to_f <=> y[1,y.size-1].to_f }
    else
      return arr.sort
    end
  end
end