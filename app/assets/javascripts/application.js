// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .
function disable() {
  document.getElementById("game_direction").disabled=true;
  document.getElementById("game_x_pos").disabled=true;
  document.getElementById("game_y_pos").disabled=true;

}
function enable() {
  document.getElementById("game_direction").disabled=false;
  document.getElementById("game_x_pos").disabled=false;
  document.getElementById("game_y_pos").disabled=false;
}

function change() {
   var decider = document.getElementById('checkbox_id').checked;
   
  if(decider){
     
     disable();

  } else {
     
     enable();
  }
}

function setTarget(obj) {
  var cols = ["One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"];
  var my_class = obj.className.split("-");
  var x = my_class[0];
  var y = cols.indexOf(my_class[1]) + 1;
  clearGrid();
  document.getElementById("game_x_pos").value = x;
  document.getElementById("game_y_pos").value = y;
  var new_class = obj.className+"-Selected";
  //console.log(new_class);
  obj.className = new_class;
  
}

function clearGrid() {
  //console.log("Clearing");
  const rows = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]
  var cols = ["One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"];
  //myVal = document.getElementById('A_Four').innerHTML;
  //console.log(myVal);
  //document.getElementById('A_Four').className = "A-Four"

  rows.forEach(function (r) {
    cols.forEach(function (c) {
      var id = r+"_"+c;
      var norm_class = r+"-"+c;
      //console.log(r, c, id, norm_class);

      document.getElementById(id).className = norm_class;

    });
    //console.log(item, index);
  });

}

function setDirection(obj) {
   //console.log("Direction clicked for "+obj.id);
   if (obj.id == "hori-z"){
      document.getElementById("game_direction").value = "V";
      document.getElementById("hori-z-div").style.display = "none";
      document.getElementById("vert-div").style.display = "block";
    }  else {
      document.getElementById("game_direction").value = "H";
      document.getElementById("hori-z-div").style.display = "block";
      document.getElementById("vert-div").style.display = "none";
    }

}

function setInit() {
  console.log(document.getElementById("game_direction").value);
  if  (document.getElementById("game_direction").value == "V") {
      
      document.getElementById("hori-z-div").style.display = "none";
      document.getElementById("vert-div").style.display = "block";
    }  else {
      
      document.getElementById("hori-z-div").style.display = "block";
      document.getElementById("vert-div").style.display = "none";
    }
}